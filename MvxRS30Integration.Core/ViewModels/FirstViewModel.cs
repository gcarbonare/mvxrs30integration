using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace MvxRS30Integration.Core.ViewModels
{
	public class FirstViewModel 
        : MvxViewModel
	{
		private string _hello = "Hello MvvmCross";

		public string Hello { 
			get { return _hello; }
			set { SetProperty (ref _hello, value); }
		}

		IMvxMessenger _messenger;

		public FirstViewModel (IMvxMessenger messenger)
		{
			_messenger = messenger;
			_messenger.Subscribe<ScanMessage> (OnScanMessageReceived);
			
		}

		void OnScanMessageReceived (ScanMessage scanMessage)
		{
			Hello = scanMessage.DecodedBarCode;
		}
	}
}
