using MvvmCross.Platform.IoC;
using MvvmCross.Platform;

namespace MvxRS30Integration.Core
{
	public class App : MvvmCross.Core.ViewModels.MvxApplication
	{
		IScannerService scannerService;

		public override void Initialize ()
		{
			CreatableTypes ()
                .EndingWith ("Service")
                .AsInterfaces ()
                .RegisterAsLazySingleton ();

			RegisterAppStart<ViewModels.FirstViewModel> ();

			scannerService = Mvx.Resolve<IScannerService> ();
			scannerService.StartService ();
		}


	}
}
