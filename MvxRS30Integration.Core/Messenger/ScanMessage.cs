﻿using System;
using MvvmCross.Plugins.Messenger;

namespace MvxRS30Integration.Core
{
	public class ScanMessage : MvxMessage
	{
		public readonly string DecodedBarCode;

		public ScanMessage (object sender, string decodedBarCode) : base (sender)
		{
			DecodedBarCode = decodedBarCode;
		}
	}
}

