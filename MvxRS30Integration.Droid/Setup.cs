using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using MvxRS30Integration.Core;

namespace MvxRS30Integration.Droid
{
	public class Setup : MvxAndroidSetup
	{
		public Setup (Context applicationContext) : base (applicationContext)
		{
		}

		protected override IMvxApplication CreateApp ()
		{
			return new Core.App ();
		}

		protected override IMvxTrace CreateDebugTrace ()
		{
			return new DebugTrace ();
		}

		protected override void InitializeApp (MvvmCross.Platform.Plugins.IMvxPluginManager pluginManager)
		{
			base.InitializeApp (pluginManager);
		}

		protected override void InitializeIoC ()
		{
			base.InitializeIoC ();
			Mvx.RegisterSingleton<IScannerService> (() => new ScannerService ());
		}
	}
}
