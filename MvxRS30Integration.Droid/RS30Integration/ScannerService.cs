﻿using System;
using Com.Cipherlab.Barcode;
using Android.Content;
using Android.App;
using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using Com.Cipherlab.Barcode.Decoderparams;
using Com.Cipherlab.Barcode.Decoder;
using MvvmCross.Plugins.Messenger;
using MvxRS30Integration.Core;

namespace MvxRS30Integration.Droid
{

	public class ScannerService :IScannerService, IRS30Barcode, BarcodeReceiver.IBarcodeReceiver
	{
		
		public ScannerService ()
		{
			
		}

		public void StartService ()
		{
			this.Initialize ();
		}

		#region IRS30Barcode implementation

		public ReaderManager mReaderManager;
		private IntentFilter filter;
		private BarcodeReceiver barcodereceiver;

		public void Initialize ()
		{
			mReaderManager = ReaderManager.InitInstance (Application.Context);

			filter = new IntentFilter ();
			filter.AddAction (GeneralString.IntentSOFTTRIGGERDATA);
			filter.AddAction (GeneralString.IntentPASSTOAPP);
			filter.AddAction (GeneralString.IntentREADERSERVICECONNECTED);

			barcodereceiver = new BarcodeReceiver (this);

			Application.Context.RegisterReceiver (barcodereceiver, filter);
			//throw new NotImplementedException ();

		}

		public void SetupBarcode ()
		{
			ReaderOutputConfiguration settings = new ReaderOutputConfiguration ();
			// mReaderManager.Get_ReaderOutputConfiguration (settings);
			settings.EnableKeyboardEmulation = KeyboardEmulationType.None;
			mReaderManager.Set_ReaderOutputConfiguration (settings);
		}

		public void UpdateBarcode (String str)
		{
			Mvx.Resolve<IMvxMessenger> ().Publish<ScanMessage> (new ScanMessage (this, str));
		}


		#endregion

		#region IBarcodeReceiver implementation

		public void Result (string result)
		{
			UpdateBarcode (result);
		}

		public void ReaderConnected ()
		{
			SetupBarcode ();
		}

		#endregion
	}
}

