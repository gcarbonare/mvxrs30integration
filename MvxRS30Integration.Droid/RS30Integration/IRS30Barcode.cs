﻿using System;

namespace MvxRS30Integration.Droid
{
	public interface IRS30Barcode
	{
		void Initialize ();

		void SetupBarcode ();

		void UpdateBarcode (string str);
	}
}

