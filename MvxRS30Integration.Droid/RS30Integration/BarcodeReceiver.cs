﻿using System;
using Android.Content;
using Com.Cipherlab.Barcode;

namespace MvxRS30Integration.Droid
{
	[BroadcastReceiver]
	public class BarcodeReceiver : BroadcastReceiver
	{
		public BarcodeReceiver ()
		{
		}

		public interface IBarcodeReceiver
		{
			void Result (String result);

			void ReaderConnected ();
		}

		private IBarcodeReceiver callback;

		public BarcodeReceiver (IBarcodeReceiver callback)
		{
			this.callback = callback;
		}

		public override void OnReceive (Context context, Intent intent)
		{
			if (intent.Action == GeneralString.IntentSOFTTRIGGERDATA)
			{
				String data = intent.GetStringExtra (GeneralString.BcReaderData);

				if (callback != null)
				{
					callback.Result (data);
				}

			}

			if (intent.Action == GeneralString.IntentPASSTOAPP)
			{
				String data = intent.GetStringExtra (GeneralString.BcReaderData);
				if (callback != null)
				{
					callback.Result (data);
				}

			}

			if (intent.Action == GeneralString.IntentREADERSERVICECONNECTED)
			{
				if (callback != null)
				{
					callback.ReaderConnected ();
				}

			} 
		}
	}
}

